/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pinkrice.foto.model

class Photo(val title: String,
            val thumb: String,
            val med: String,
            val original: String,
            val published: String) {}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pinkrice.foto.model

import com.pinkrice.foto.lib.Skydrive

object Album {
  def urlFromId(id: String) = Skydrive.baseUrl + "/Folders('" + id + "')/"
}

class Album(val title: String,
            val thumb: String,
            val id: String,
            val count: Int) {}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pinkrice.foto.snippet

import xml.NodeSeq
import net.liftweb.util.Helpers
import net.liftweb.util.Helpers._
import com.pinkrice.foto.lib._

class Home {

  // Show Albums

  def showAlbums(html: NodeSeq): NodeSeq = {
    bind("album", html,
         "list" -> bindAlbumList _
    )
  }

  // Bind Album List

  def bindAlbumList(html: NodeSeq): NodeSeq = {
    Skydrive.getAlbums.flatMap(
      album => {
        val zindex = Helpers.randomInt(100)
        val rotate = Helpers.randomInt(30) - 15
        bind("album", html,
          "thumb" ->
            <a href={"/album?id="+album.id} style={"z-index:"+zindex+";-webkit-transform:rotate("+rotate+"deg);-moz-transform: rotate("+rotate+"deg);"}>
              <img src={album.thumb} alt={album.title} />
            </a>
        )
      }
    )
  }

}

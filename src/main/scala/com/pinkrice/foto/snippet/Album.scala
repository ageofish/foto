/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pinkrice.foto.snippet

import xml.NodeSeq
import com.pinkrice.foto.lib._
import net.liftweb.util.Helpers._
import net.liftweb.http.S

class Album {

  lazy val albumid = S.param("id").openOr("C64310FF4BFC24F6!479")
  lazy val (albumtitle, photos) = Skydrive.getAlbumDetail(albumid)

  // Show Title

  def showTitle(html: NodeSeq): NodeSeq = {
    bind("album", html,
         "title" -> albumtitle)
  }

  // Show Photos

  def showPhotos(html: NodeSeq): NodeSeq = {
    bind("photo", html,
         "list" -> bindPhotoList _
    )
  }

  def bindPhotoList(html: NodeSeq): NodeSeq = {
    photos.flatMap( photo => {
        bind("photo", html,
                    "thumb" ->
                      <a class="thumb" name="leaf" href={photo.med} >
                        <img src={photo.thumb} />
                      </a>
                      <div class="caption">
                        {photo.title}
                      </div>)
      }
    )
  }

}

/*
 * foto:XmlFetcher 2010(c) pinkrice
 */

package com.pinkrice.foto.lib

import xml.NodeSeq
import com.pinkrice.foto.model._

object Skydrive {
  private val _userId = "cid-c64310ff4bfc24f6"
  private val _userNumId = "Users(-4160462942082685706)"
  private val _albumsUrl = "http://" + _userId + ".users.api.live.net/" + _userNumId + "/Files"

  def baseUrl = _albumsUrl

  // Get Albums

  def getAlbums() = {

    println(_albumsUrl) // debug

    val albumsraw: NodeSeq = XmlFetcher.fetch(_albumsUrl) \\ "entry"

    albumsraw.filter( x => ((x \ "category") == "Photos" && (x \ "itemCount") != "0"))
      .map( x => {
        new Album((x \ "title").text,
                  ((x \ "thumbnail") \ "@url").text,
                  (x \ "resourceId").text,
                  (x \ "itemCount").text.toInt)
      } )
  }

  // Get Album Detail

  def getAlbumDetail(albumid: String) = {

    val xmlraw = XmlFetcher.fetch( Album.urlFromId(albumid) )
    val photosraw = xmlraw \ "entry"

    ((xmlraw \ "title").text,
    photosraw.map( p =>
      new Photo((p \ "title").text,
                (((p \ "thumbnail").filter(t => ((t \ "@{http://api.live.com/schemas}maxWidth").text == "48"))(1)) \ "@url").text,
                (((p \ "thumbnail").filter(t => ((t \ "@{http://api.live.com/schemas}maxWidth").text == "600"))(0)) \ "@url").text,
                ((p \ "content") \ "@src").text,
                (p \ "updated").text)
    ))
  }
  
}

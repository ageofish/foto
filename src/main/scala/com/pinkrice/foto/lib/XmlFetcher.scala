/*
 * foto:XmlFetcher 2010(c) pinkrice
 */

package com.pinkrice.foto.lib

import xml.{Node, XML}
import java.net.{URL, HttpURLConnection}

object XmlFetcher {

  // Fetch XML from a URL
  
  def fetch(urlStr: String): Node = {
    val conn = (new URL(urlStr)).openConnection.asInstanceOf[HttpURLConnection]
    XML.load(conn.getInputStream())
  }

}
